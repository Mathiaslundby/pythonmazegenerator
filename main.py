import pygame
import maze
import sys
from tile import tile_size
from colors import *


# Sets default maze width and height, in case no arguments are given
width = 79
height = 79


# Sets width and height to parameters if arguments are given
args = sys.argv
if len(args) > 2:
    try:
        width = int(args[1])
        height = int(args[2])
    except ValueError:
        print(ValueError)
        pass

# The maze generator only works on a grid that has an odd number of tiles
# If it was an even number it would have multiple exits and it would look weird
if width % 2 == 0:
    width += 1
if height % 2 == 0:
    height += 1


# Set max amount of tiles in a given direction
# A recursive function is used to generate the maze,
# and we would reach the max amount of recursions on mazes bigger than 80x80
if width > 80:
    width = 79
if height > 80:
    height = 79


# Set tile size based on amount of tiles in a given direction
if height > 50 or width > 50:
    tile_size = 10
elif height > 30 or width > 30:
    tile_size = 15
elif height > 25 or width > 25:
    tile_size = 20
elif height > 20 or width > 20:
    tile_size = 25
elif height > 15 or width > 15:
    tile_size = 30
elif height > 10 or width > 10:
    tile_size = 35
elif height > 5 or width > 5:
    tile_size = 40


# Calculate the window size
# (tile_size * 2) gives a padding on both sides, the same size as the tiles
# (tiles_size * width) gives room for the tiles them selves, same with height
# the add the width/height, to make room for the paddings between each tile
window_width = (tile_size * 2) + (tile_size * width) + width
window_height = tile_size * 2 + tile_size * height + height

# Initialize pygame
pygame.init()

# Set up the drawing window
screen = pygame.display.set_mode([window_width, window_height])

# Paint the background light gray
screen.fill(light_gray)

# Create a shell of the maze
matrix = maze.maze_shell(width, height, tile_size)

# Draw each tile of the maze
for row in matrix:
    for tile in row:
        pygame.draw.rect(screen, tile.color, tile.rect)

# Update the window to render the tiles
pygame.display.flip()

# Generate a random maze
maze.generate_maze(screen, matrix)


refresh = 0
# Run until the user asks to quit
running = True
while running:
    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # refresh += 1
    # if refresh == 100:
    #    refresh = 0

    # Refresh window
    # pygame.display.flip()


# Done! Time to quit.
pygame.quit()
