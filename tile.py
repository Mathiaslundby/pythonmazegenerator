import pygame
from colors import *


tile_size = 20


# A class for tiles, storing the pygame rectangle, x and y position, tile type
# visited attribute is used in when generating and solving the mazes
class Tile:

    rect = pygame.Rect(0, 0, 0, 0)
    x = 0
    y = 0
    tile_type = 0
    visited = False

    # Constructor for the object
    def __init__(self, x, y, tile_type):
        self.x = x
        self.y = y
        self.tile_type = tile_type
        self.rect = pygame.Rect(x, y, tile_size, tile_size)

        # When tiles are created they are either gray, indicating a wall
        # or green, indicating the exit point
        if tile_type == 1:
            self.color = gray
        elif tile_type == 2:
            self.color = green
