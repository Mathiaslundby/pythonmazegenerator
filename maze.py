import numpy as np
import random
import time
import pygame
from colors import *
import tile


def maze_shell(width, height, size):

    tile.tile_size = size
    maze = np.ones([height, width], dtype=int)

    # 1 represents walls
    # 2 represents exit

    # Create a random exit point along the bottom
    # Making sure it is connected to a (unvisited) path tile
    exit_point = 0
    while exit_point % 2 == 0:
        exit_point = random.randint(1, len(maze[0]) - 2)

    maze[len(maze) - 1][exit_point] = 2

    # Creates an array of tiles, using the generated info above
    # Setting the tiles' positions based on their size, then adding a padding of 2 pixels for a nice looking grid
    tiles = []
    y = 0
    for row in maze:
        y += tile.tile_size + 1
        x = 0
        for num in row:
            x += tile.tile_size + 1
            tiles.append(tile.Tile(x, y, num))

    # Reshaping the array back into a 2d array
    tile_maze = np.array(tiles).reshape((height, width))

    return tile_maze


def generate_maze(screen, maze):

    # Find starting point for generator (Exit point in maze)
    start_x = 0
    bottom = len(maze) - 1
    for tile in maze[bottom]:
        if tile.tile_type == 2:
            break
        start_x += 1
    start_y = bottom - 1

    # Find first tile and set visited to True
    first_tile = maze[start_y][start_x]
    first_tile.tile_type = 0
    first_tile.visited = True

    # Draw first path tile
    pygame.draw.rect(screen, blue, first_tile)

    # Recursive function that finds the next tile for the maze
    def move(x, y, wall, prev):
        # Uses x and y coordinate to find the current tile. Mark as visited
        current = maze[y][x]
        current.tile_type = 0
        wall.tile_type = 0
        current.visited = True

        # Draw current tile blue, previous tile white, and wall between these two tiles white
        pygame.draw.rect(screen, blue, current)
        pygame.draw.rect(screen, white, prev)
        pygame.draw.rect(screen, white, wall)
        pygame.display.flip()
        # Sleep for a given time to make visualization easier
        time.sleep(0.03)

        # Shuffle the four possible directions
        directions = [(x - 2, y), (x, y + 2), (x + 2, y), (x, y - 2)]
        random.shuffle(directions)

        # Loop through the directions and move to the next one if possible
        for (xx, yy) in directions:

            # When the next tile is out of bounds or already visited, try a new direction
            # When all directions are tried, the recursion will end
            if xx >= len(maze[0]) or xx < 0 or yy >= len(maze) or yy < 0 or maze[yy][xx].visited:
                # Make sure the end tiles are painted white again
                pygame.draw.rect(screen, white, current)
                continue

            # Get wall between current tile and the next tile
            wall = get_wall(maze, x, y, xx, yy)
            # Move to next tile
            move(xx, yy, wall, current)

            # When The recursion ends, paint the last visited tile white again
            pygame.draw.rect(screen, white, current)
            pygame.display.flip()


    # Get a random valid starting direction for the maze to start generating a path
    # Exit point is always along the bottom, downwards is not an option
    first_direction = [(start_x - 2, start_y), (start_x + 2, start_y), (start_x, start_y - 2)]
    random.shuffle(first_direction)
    for (x_dir, y_dir) in first_direction:
        if x_dir >= len(maze[0]) or x_dir < 0 or y_dir < 0:
            continue

        # Find the first wall to become a path
        first_wall = get_wall(maze, start_x, start_y, x_dir, y_dir)
        # Move towards the first tile
        move(x_dir, y_dir, first_wall, first_tile)
        break


def get_wall(maze, x, y, xx, yy):
    # When xx is equal to x we move vertical
    if xx == x:
        # When yy is less than y we move upwards
        if yy < y:
            wall = maze[y - 1][x]
        # Else the move is downwards
        else:
            wall = maze[y + 1][x]

    # If xx is not equal to x the move is horizontal
    else:
        # When xx is less than x we move to the left
        if xx < x:
            wall = maze[y][x - 1]
        else:
            wall = maze[y][x + 1]

    return wall
